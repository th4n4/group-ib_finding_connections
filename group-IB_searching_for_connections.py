# %%
#first we will begin by importing all of our needed libraries and frameworks.
import time #we only really use this to throttle some api requests

import pandas as pd
import numpy as np
import requests

from tabulate import tabulate

import plotly.graph_objects as go
import plotly.express as px


# %%
#this function does a request to ip-API and obtains the desired information. We could probably obtain a bit more information
#but that was unasked for. If we wanted to scale upwords we should not use the free plan, we are currently limited to 45 requests per minute
# anything over that is throttled.
#documentation is crystal clear: http://ip-api.com/json/{query} and we then just extract the fields that were asked for.

def get_ip_info(ip_address):
    url = f"http://ip-api.com/json/{ip_address}"
    response = requests.get(url)
    if response.status_code == 200:
        try:
            data = response.json()
            return {
                'country': data.get('country', ''),
                'city': data.get('city', ''),
                'subnet': data.get('isp', ''), # ip-api does not provide subnet, using ISP as a fallback
                'time_zone': data.get('timezone', ''),
            }
        except ValueError:
            print(f"Failed to parse JSON for IP: {ip_address}")
            return {}
    else:
        print(f"Failed to fetch data for IP: {ip_address}")
        return {}

# %%
#this function does an API request to nominatim, based on city and country for the purpouses of obtaining the latitude and longitude of that city.
#we are only requested to 1 request per second. once again this is due to the free plan. We are using this one as it does not require registration nor api keys
#for this tier so we don't have to deal with .env files which might be complicated for evaluations, and possible leakage of personal details.

def get_geolocation(city, country):
    url = f"https://nominatim.openstreetmap.org/search?city={city}&country={country}&format=json"
    response = requests.get(url)
    if response.status_code == 200:
        try:
            data = response.json()
            if data:
                return data[0]['lat'], data[0]['lon']
            else:
                return None, None
        except ValueError:
            print(f"Failed to parse JSON for city: {city}, country: {country}")
            return None, None
    else:
        print(f"Failed to fetch data for city: {city}, country: {country}")
        return None, None

# %%
#just a simple function that allows us too differentiate when we are dealing with a mobile device. 
#the logic is as follows: if the os field contains iOS or Android we will return 1, if the word mobile can be found in the browser we will also
#return 1

def is_mobile_device(os_field, browser_field):
    if isinstance(os_field, str) and ("iOS" in os_field.lower() or "Android" in os_field.lower()):
        return 1
    elif isinstance(browser_field, str) and "mobile" in browser_field.lower():
        return 1
    else:
        return 0


# %%
##function for grouping stuff to be used when making the pretty world map
def aggregate_data(group):
    return group[['device_id', 'identity', 'ips']].to_dict('records')


# %%
#here we have 2 functions that we will use to help us identify and flag compromised devices and compromised accounts. 
#all they do is check if a field is equal to something else. if this was mroe complicated we might want too converet them to process a list

#these functions are now depreceated, and we don't use them anymore but we are keeping them as cargo code

def is_wanted_identity(identity, wanted_identity, bank, wanted_bank):
    if identity == wanted_identity and bank == wanted_bank:
        return 1
    else:
        return 0

def is_wanted_device(device, wanted_device, bank, wanted_bank):
    if device == wanted_device and bank == wanted_bank:
        return 1
    else:
        return 0


# %%
#some special variables we might need eventually

compromised_device = '91b12379-8098-457f-a2ad-a94d767797c2' #bank4

compromised_identity = '0007f265568f1abc1da791e852877df2047b3af9' #bank8


#we might need to add bank 4 and bank 8 possibly

# %%
#Just doing the basic File loading section

df = pd.read_csv('test.csv') # loading the CSV omtp a data frame


#print(df.head()) #printing  the head it so we can see what its like

# %%
#defining the master dataframe that we will finish populating later and that we pretty much use for everything.

master_df = df.copy() # just copying it, this will be our master dataframe

# Separating it in such a way that each IP address is exploded.
master_df['ips'] = master_df['ips'].str.split(',')
master_df = master_df.explode('ips')

# Creating empty columns in the DataFrame that we will populate later.
master_df['country'] = ''
master_df['city'] = ''
master_df['subnet'] = ''
master_df['time_zone'] = ''
master_df['is_mobile'] = 0 #not mobile is default, then we will go over with some criterion and flag the ones that are mobile with a 1.


# %%
#in this section we will populate the master_df with the missing fields

#just a formality, still nice to know
total_rows = len(master_df)
counter = 0

#iterating through every row, and obtaining the ip address and doing a request using our function then we will populate the other fields
#this takes about 13 to 9 minutes, biggest throttle is the requests per minute.
for index, row in master_df.iterrows():
    ip_address = row['ips']
    ip_info = get_ip_info(ip_address)
    
    master_df.at[index, 'country'] = ip_info.get('country', '')
    master_df.at[index, 'city'] = ip_info.get('city', '')
    master_df.at[index, 'subnet'] = ip_info.get('subnet', '')
    master_df.at[index, 'time_zone'] = ip_info.get('time_zone', '')

    #think of this too see how more do we need
    counter += 1 
    print(f"{counter} out of {total_rows} requests done.")

    #print(f"Current row: {row}") # just too test if this works, it does work

# %%
#applying the is_mobile_device to each row to fill the is_mobile column
master_df['is_mobile'] = master_df.apply(lambda row: is_mobile_device(row['os'], row['browser']), axis=1)

#print(master_df) making sure it works... it does

# %%
#in this section we will extract columns and reorder a new data frame with the purpouse of exporting it as csv.
#one with ips, county, city, subnet, timezone,  OS +version, browser + version, is_mobile

export_df = master_df.copy()
export_df = export_df.drop(['device_id', 'identity', 'bank', 'device_fingerprint', 'gpu_renderers', 'screen'], axis=1)

export_df.to_csv('export_df.csv', index=False)


# %%
#in this section we will find compromised operations. We will add 3 columns to act as descriptors 1 for compromised device,
#and another for compromised account, and a third one if a field is duplicated anyways. just setting them to empty for cleanliness.

master_df['device_id_compromised'] = ''
master_df['identity_compromised'] = ''

#the banks are hardcoded but thats alright
master_df['device_id_compromised'] = master_df.apply(lambda row: 1 if row['device_id'] == compromised_device and row['bank'] == 'Bank4' else row['device_id_compromised'], axis=1)

master_df['identity_compromised'] = master_df.apply(lambda row: 1 if row['identity'] == compromised_identity and row['bank'] == 'Bank8' else row['identity_compromised'], axis=1)





# %%
#just saving stuff into csv's just in case

#print(master_df)

master_df.to_csv('master.csv', index=False)

compromised_entries_df = master_df[(master_df['device_id_compromised'] == 1) | (master_df['identity_compromised'] == 1)]
print(compromised_entries_df)

compromised_entries_df.to_csv('compromised.csv', index=False)

# %%

master_df['latitude'] = None
master_df['longitude'] = None

#list of cities and their country
city_data = master_df[['city', 'country', 'latitude', 'longitude']].drop_duplicates().to_dict('records')


for city_info in city_data: #takes about 2 m 15 s on my machine, most limiting factor is requests/ sec
    city = city_info['city']
    country = city_info['country']
    
    # get lat and long
    latitude, longitude = get_geolocation(city, country)
    
    # update city_info with lat and long
    city_info['latitude'] = latitude
    city_info['longitude'] = longitude
    
    # do nothign for a bit over a sec to avoid hitting the rate limit
    time.sleep(1.2)




# Create a world map with city data points


#fig.show()



# %%
#in this part we will do the world graph

printable_labels = master_df.copy() 
#if you want to show more info you could just remove a thing from the columns being dropped, but doing it like this maintains cleanliness
printable_labels = printable_labels.drop(['gpu_renderers', 'screen', 'bank', 'is_mobile', 'device_id_compromised', 'identity_compromised', 'browser', 'os', "device_fingerprint", 'time_zone', 'subnet', 'latitude', 'longitude'], axis=1)

#making a set for the cities where compromised devices or identitys have been found
compromised_cities = set(compromised_entries_df['city'])

#doing the hoverdata stuff
hover_data = printable_labels.groupby('city').apply(aggregate_data).to_dict()

hover_data_str = {city: '<br>'.join([f"device_id: {row['device_id']}, identity: {row['identity']}, ips: {row['ips']}" for row in rows]) for city, rows in hover_data.items()}


customdata=[hover_data_str.get(city['city'], 'No additional data') for city in city_data]

# getting important information from city_data
latitudes = [city['latitude'] for city in city_data]
longitudes = [city['longitude'] for city in city_data]
city_names = [city['city'] for city in city_data]

# just doing some stuff for colors
colors = ['red' if city['city'] in compromised_cities else 'blue' for city in city_data]

# hovering scatterplot over geography
scatter = go.Scattergeo(
    lat=[city['latitude'] for city in city_data],
    lon=[city['longitude'] for city in city_data],
    text=[city['city'] for city in city_data],
    mode='markers',
    marker=dict(
        size=8,
        color=colors, # Use the colors list here
        opacity=0.8,
        symbol='circle',
        line=dict(
            width=1,
            color='rgba(102, 102, 102)'
        )
    ),
    hovertemplate=
    "<b>%{text}</b><br>" +
    "%{customdata}<br>" +
    "<extra></extra>",
    customdata=[hover_data_str.get(city['city'], 'No additional data') for city in city_data]
)

# making the layout
layout = go.Layout(
    title="World Map Cities in Which Transactions Were Recorded",
    geo=dict(
        showland=True,
        showlakes=True,
        showocean=True,
        showcountries=True,
        showcoastlines=True,
        projection_type="natural earth",
        resolution=50,
        center=dict(
            lat=0,
            lon=0
        ),
        lataxis=dict(range=[-90, 90]),
        lonaxis=dict(range=[-180, 180]),
        landcolor="rgb(204, 255, 111)",
        lakecolor="rgb(0, 200, 255)", #no idea what i'm doing with colors
        oceancolor="rgb(0, 0, 122)",
        countrycolor="rgb(204, 204, 204)",
        coastlinecolor="rgb(102, 102, 102)",
        bgcolor="rgb(243, 243, 243)",
        subunitcolor="rgb(255, 255, 255)",
        countrywidth=0.5,
        subunitwidth=0.5
    ),
    width=1200, # 
    height=1000, # might be good to make this a proper screen size
    margin=dict(l=50, r=50, t=100, b=100) #
)


fig = go.Figure(data=[scatter], layout=layout)
fig.show()



# %%
#making some other nice pretty charts here a bar chart for activity by city

city_counts = master_df.groupby('city').size().reset_index(name='activity')
fig2 = px.bar(city_counts, x='city', y='activity', title='Activity by City')
fig.update_yaxes(type='log')#this did less than i expected.
fig2.show()


# %%
#this is interesting since we have a few massive outliers

identity_counts = master_df.groupby('identity').size().reset_index(name='activity')
identity_counts = identity_counts.sort_values(by='activity', ascending=False)
fig3 = px.bar(identity_counts, x='identity', y='activity', title='Total Occurrences by Identity')
fig3.show()





# %%
#we have a similar situation as above
device_id_counts = master_df.groupby('device_id').size().reset_index(name='activity')
device_id_counts = device_id_counts.sort_values(by='activity', ascending=False)
fig4 = px.bar(device_id_counts, x='device_id', y='activity', title='Total Occurrences by Device ID')
fig4.show()



# %%
#lets get some basic statistics, dont then we can really use them
device_id_counts = master_df.groupby('device_id').size()
identity_counts = master_df.groupby('identity').size()
ips_counts = master_df.groupby('ips').size()



#stuff for device id
device_id_mean = np.mean(device_id_counts)
device_id_median = np.median(device_id_counts)
device_id_mode = device_id_counts.mode()[0] if not device_id_counts.mode().empty else None

#stuff for identity
identity_mean = np.mean(identity_counts)
identity_median = np.median(identity_counts)
identity_mode = identity_counts.mode()[0] if not identity_counts.mode().empty else None

#stuff for ips
ips_mean = np.mean(ips_counts)
ips_median = np.median(ips_counts)
ips_mode = ips_counts.mode()[0] if not ips_counts.mode().empty else None

print("Device ID Statistics:")
print(f"Mean: {device_id_mean}")
print(f"Median: {device_id_median}")
print(f"Mode: {device_id_mode}")

print("\nIdentity Statistics:")
print(f"Mean: {identity_mean}")
print(f"Median: {identity_median}")
print(f"Mode: {identity_mode}")

print("\nIPs Statistics:")
print(f"Mean: {ips_mean}")
print(f"Median: {ips_median}")
print(f"Mode: {ips_mode}")

# %%
#how many mobile devices
total_mobile_devices = master_df['is_mobile'].sum()
print(f"Total Mobile Devices: {total_mobile_devices}")


# %%
#there is no way we can fint this into a page
compromised_entries_df_to_latex = compromised_entries_df.copy()

latex_table = tabulate(compromised_entries_df_to_latex, tablefmt="latex", headers="keys")
print(latex_table)



